package com.zarupa.irem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IremApplication {
    public static void main(String[] args) {
        SpringApplication.run(IremApplication.class, args);
    }

}
